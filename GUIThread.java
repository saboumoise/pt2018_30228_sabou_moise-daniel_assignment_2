import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class GUIThread implements Runnable{

    private int numberOfQueues;
    private Shop myShop;

    public GUIThread(int numberOfQueues, Shop myShop) {
        this.numberOfQueues = numberOfQueues;
        this.myShop = myShop;
    }

    public void run()
    {
        JFrame outputFrame = new JFrame("Output Frame");
        outputFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        outputFrame.setSize(1000, 700);

        JPanel outputMainPanel = new JPanel();
        outputMainPanel.setPreferredSize(new Dimension(950, 500));
        JPanel queuesPanel = new JPanel();
        JPanel logPanel = new JPanel();

        queuesPanel.setLayout(new GridLayout(numberOfQueues, 1));

        ArrayList<JTextField> queuesFields = new ArrayList<>();
        for (int i = 0; i < numberOfQueues; i++)
        {
            JTextField currentTextField = new JTextField();
            currentTextField.setPreferredSize(new Dimension(900, 30));
            queuesFields.add(currentTextField);
            queuesPanel.add(currentTextField);
        }

        outputMainPanel.add(queuesPanel);
        outputFrame.add(outputMainPanel);
        outputFrame.setVisible(true);

        while(myShop.getClosed().getValue().equals(false))
        {
            for(int i = 0; i < numberOfQueues; i++)
            {
                queuesFields.get(i).setText(myShop.getAllQueues().get(i).toString());
            }
            queuesPanel.removeAll();
            for(JTextField i : queuesFields)
            {
                queuesPanel.add(i);
            }
            queuesPanel.revalidate();
            queuesPanel.repaint();
        }

        JTextField logTextField = new JTextField("Peak Time = " + myShop.getPeakTime());
        JTextField logTextField1 = new JTextField("Average Waiting Time = " + myShop.getAverageWaitingTime());

        logPanel.add(logTextField);
        logPanel.add(logTextField1);

        outputMainPanel.add(logPanel);
        outputFrame.setVisible(true);
    }
}
