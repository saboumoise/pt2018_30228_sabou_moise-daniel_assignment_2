public class QueueThread implements Runnable {

    private Bool running;
    private int queueIndex;
    private Shop myShop;

    public QueueThread(Bool running,int queueIndex, Shop myShop)
    {
        this.running = running;
        this.queueIndex = queueIndex;
        this.myShop = myShop;
    }

    public void run()
    {
        System.out.println("Thread" + queueIndex + "created");
        while (running.getValue() || !myShop.getAllQueues().get(queueIndex).isEmpty())
        {
            myShop.productScan(queueIndex);
        }
        System.out.println("Thread" + queueIndex + "ended");
    }
}
