import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class Shop {
    private int numberOfQueues;
    private int simulationInterval;
    private int minServiceTime;
    private int maxServiceTime;
    private int minArrivalTime;
    private int maxArrivalTime;
    private int clientIndex;
    private volatile Bool running;
    private ArrayList<Queue<Client>> allQueues;
    private ArrayList<Client> allClients;
    private int peakTime;
    private int peakValue;
    private Bool closed;

    public Shop(int numberOfQueues,
                int simulationInterval,
                int minServiceTime,
                int maxServiceTime,
                int minArrivalTime,
                int maxArrivalTime)
    {
        this.numberOfQueues = numberOfQueues;
        this.simulationInterval = simulationInterval;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
        this.minArrivalTime = minArrivalTime;
        this.maxArrivalTime = maxArrivalTime;
        this.running = new Bool(true);
        this.allQueues = new ArrayList<>();
        this.allClients = new ArrayList<>();
        this.peakTime = simulationInterval;
        this.peakValue = 0;
        this.closed = new Bool(false);

        for (int i = 0; i < numberOfQueues; i++)
        {
            Queue<Client> newQueue = new LinkedList<>();
            this.allQueues.add(newQueue);
        }
    }

    public ArrayList<Queue<Client>> getAllQueues() {
        return allQueues;
    }

    public int getPeakTime() {
        return peakTime;
    }

    public void open()
    {
        Random rand = new Random();
        int timeOfArrivalNextClient = 0;

        ArrayList<Thread> threads = new ArrayList<>();

        for (int i = 0; i < numberOfQueues; i++)
        {
            QueueThread newQueueThread = new QueueThread(running, i, this);
            Thread newThread = new Thread(newQueueThread);
            threads.add(newThread);
        }

        for (Thread i : threads)
        {
            i.start();
        }

        while(simulationInterval > 0)
        {
            try {
                Thread.sleep(1000);
            }catch (InterruptedException ie)
            {
                System.out.println(ie);
            }
            simulationInterval--;
            updatePeakTime();
            System.out.println("time = " + simulationInterval);
            System.out.println(allQueues);
            if (timeOfArrivalNextClient == 0)
            {
                clientArrival();
                timeOfArrivalNextClient = rand.nextInt(maxArrivalTime - minArrivalTime) + minArrivalTime;
            }
            else
            {
                timeOfArrivalNextClient--;
            }
        }
        running.setValue(false);

        for (Thread i : threads)
        {
            try {
                i.join();
            }catch (InterruptedException ie)
            {
                System.out.println(ie);
            }
        }
        closed.setValue(true);
    }

    public Bool getClosed() {
        return closed;
    }

    public synchronized void productScan(int index)
    {
        Queue<Client> currentQueue = allQueues.get(index);
        if (!currentQueue.isEmpty())
        {
            if (currentQueue.element().getNumberOfProducts() <= 0)
            {
                Client currentClient = currentQueue.remove();
                allClients.add(currentClient);
                try {
                    Thread.sleep(900);
                } catch (InterruptedException ie)
                {
                    System.out.println(ie);
                }
            }
            else
            {
                currentQueue.element().decNumberOfProducts();
                for (Client i : currentQueue)
                {
                    i.incWaitingTime();
                }
                try {
                    Thread.sleep(900);
                } catch (InterruptedException ie)
                {
                    System.out.println(ie);
                }
            }
        }
    }

    private synchronized void clientArrival()
    {
        Random rand = new Random();
        int numberOfProducts = rand.nextInt(maxServiceTime - minServiceTime) + minServiceTime;
        Client currentClient = new Client(clientIndex++, numberOfProducts);
        allQueues.get(minClientsNumberQueue()).add(currentClient);
    }

    private int minClientsNumberQueue()
    {
        int min = allQueues.get(0).size();
        int index = 0;
        for (Queue i : allQueues)
        {
            if(i.size() < min)
            {
                min = i.size();
                index = allQueues.indexOf(i);
            }
        }
        return index;
    }

    public float getAverageWaitingTime()
    {
        int sum = 0;
        for (Client i : allClients)
        {
            sum += i.getWaitingTime();
        }
        return sum/allClients.size();
    }

    public int getNumberOfClientsWaiting()
    {
        int sum = 0;
        for (Queue i : allQueues)
        {
            sum += i.size();
        }

        return sum;
    }

    public void updatePeakTime()
    {
        if (getNumberOfClientsWaiting() > peakValue)
        {
            peakValue = getNumberOfClientsWaiting();
            peakTime = simulationInterval;
        }
    }
}
