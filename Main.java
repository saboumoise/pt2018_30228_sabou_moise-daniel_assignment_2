import javax.swing.*;
import java.awt.event.ActionEvent;

public class Main {
    public static void main(String[] args){
        JFrame controlFrame = new JFrame("Control Frame");
        controlFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        controlFrame.setSize(900, 200);

        JPanel controlInputPanel = new JPanel();

        JTextField numberOfQueuesField = new JTextField("Number of Queues");
        JTextField simulationIntervalField = new JTextField("Simulation Interval");
        JTextField minServiceTimeField = new JTextField("Minimum Service Time");
        JTextField maxServiceTimeField = new JTextField("Maximum Service Time");
        JTextField minArrivalTimeField = new JTextField("Minimum Arrival Time");
        JTextField maxArrivalTimeField = new JTextField("Maximum Arrival Time");

        controlInputPanel.add(numberOfQueuesField);
        controlInputPanel.add(simulationIntervalField);
        controlInputPanel.add(minServiceTimeField);
        controlInputPanel.add(maxServiceTimeField);
        controlInputPanel.add(minArrivalTimeField);
        controlInputPanel.add(maxArrivalTimeField);

        JPanel controlButtonPanel = new JPanel();
        JButton butonul = new JButton(new AbstractAction("OPEN") {
            @Override
            public void actionPerformed(ActionEvent e) {
                int numberOfQueues = Integer.parseInt(numberOfQueuesField.getText());
                int simulationInterval = Integer.parseInt(simulationIntervalField.getText());
                int minServiceTime = Integer.parseInt(minServiceTimeField.getText());
                int maxServiceTime = Integer.parseInt(maxServiceTimeField.getText());
                int minArrivalTime = Integer.parseInt(minArrivalTimeField.getText());
                int maxArrivalTime = Integer.parseInt(maxArrivalTimeField.getText());

                Shop myShop = new Shop(numberOfQueues, simulationInterval, minServiceTime, maxServiceTime, minArrivalTime, maxArrivalTime);

                GUIThread newGUIThread = new GUIThread(numberOfQueues, myShop);

                (new Thread(newGUIThread)).start();

                myShop.open();
            }
        });

        controlButtonPanel.add(butonul);
        JPanel mainControlPanel = new JPanel();
        mainControlPanel.add(controlInputPanel);
        mainControlPanel.add(controlButtonPanel);

        controlFrame.add(mainControlPanel);
        controlFrame.setVisible(true);
    }
}