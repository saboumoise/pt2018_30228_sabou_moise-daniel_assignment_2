public class Client {
    private int index;
    private int numberOfProducts;
    private int waitingTime;

    public Client(int index, int numberOfProducts) {
        this.index = index;
        this.numberOfProducts = numberOfProducts;
        this.waitingTime = 0;
    }

    public int getIndex() {
        return index;
    }

    public int getNumberOfProducts() {
        return numberOfProducts;
    }

    public void setNumberOfProducts(int numberOfProducts) {
        this.numberOfProducts = numberOfProducts;
    }

    public void decNumberOfProducts(){
        setNumberOfProducts(getNumberOfProducts() - 1);
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void incWaitingTime() { setWaitingTime(getWaitingTime() + 1);}

    public String toString()
    {
        return "Client" + getIndex() + " (" + getNumberOfProducts() + ")";
    }
}
