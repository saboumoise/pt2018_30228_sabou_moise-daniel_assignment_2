public class Bool {
    private Boolean value;

    public void setValue(Boolean value) {
        this.value = value;
    }

    public Boolean getValue() {

        return value;
    }

    public Bool(Boolean value) {

        this.value = value;
    }
}
